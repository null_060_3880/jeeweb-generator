有兴趣请关注主项目：JeeWeb(https://gitee.com/dataact/jeeweb)
<br />
jeeweb-generator
-----------------------------------

* 	QQ交流群： 570062301（满）、522959928
* 	官方网站： [https://www.jeeweb.cn](https://www.jeeweb.cn)
* 	项目演示： [https://demo.jeeweb.cn](https://demo.jeeweb.cn)

文档后续补上
------------
	
启动方法
-----------------------------------
    1.下载代码
    2.导入mysql.sql的SQL
    3.运行 GeneratorBootApplication
    4.访问 localhost:5001
    5.登录 admin/admin (登录可以在配置文件中更改)

使用方法
-----------------------------------

    1.在数据源管理,新建数据库连接
    2.模板方案中，添加自己的模版方案
    4.表单配置中，数据库导入（推荐数据库生成收导入，也可以在线设计）
    5.代码生成,选择想要生成的表

界面演示
----------------------------------------------------------------------------------
**数据源管理**
![输入图片说明](https://images.gitee.com/uploads/images/2018/1008/113618_0f789776_1394985.png "屏幕截图.png")
**模版方案**
![输入图片说明](https://images.gitee.com/uploads/images/2018/1008/113755_779ff23e_1394985.png "屏幕截图.png")
**模版方案制作**
模版方案采用freemarker模版引擎
![输入图片说明](https://images.gitee.com/uploads/images/2018/1008/113850_08a1e5e8_1394985.png "屏幕截图.png")
**表单配置**
![输入图片说明](https://images.gitee.com/uploads/images/2018/1008/113923_78d527d9_1394985.png "屏幕截图.png")
**代码生成**
![输入图片说明](https://images.gitee.com/uploads/images/2018/1008/114342_be724e43_1394985.png "屏幕截图.png")
<br />
**如何交流、反馈、参与贡献？** 
- Git仓库：https://gitee.com/dataact/jeeweb-generator
- [JeeWeb](https://www.jeeweb.cn)：https://www.jeeweb.cn
- 官方QQ群：570062301（满）、522959928
- 技术讨论、二次开发等咨询、问题和建议，请移步到JeeWeb开源社区，我们将会一一的解答和回复
- 如果感兴趣请Watch、Star项目，同时也是对项目最好的支持